qt_tutorial.pdf: main.tex
	xelatex -output-directory=build -jobname=qt_tutorial $<

clean:
	rm -rf build/*
