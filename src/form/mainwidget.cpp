/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mainwidget.h"
#include "ui_mainwidget.h"
#include <QPainter>
#include <QPixmap>

/**
 * @brief Class constructor
 * Creates all of the child widgets for this window.
 * @param parent    The parent widget (should be nullptr)
 */
MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    // Create the child widgets, as defined by the form.
    ui->setupUi(this);

    // Connect the valueChanged() signal of every scroll bar to the
    // updateColour() slot of this widget.
    connect(ui->redBar_, SIGNAL(valueChanged(int)),
            this, SLOT(updateColour()));
    connect(ui->greenBar_, SIGNAL(valueChanged(int)),
            this, SLOT(updateColour()));
    connect(ui->blueBar_, SIGNAL(valueChanged(int)),
            this, SLOT(updateColour()));
}

/**
 * @brief Class destructor
 */
MainWidget::~MainWidget()
{
    delete ui;
}

/**
 * @brief Handle window resizing (including the initial one on creation)
 * Since updateColour() draws an image the size of the label, it needs to
 * be called when the size changes.
 * @param event Event data
 */
void MainWidget::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    updateColour();
}

/**
 * @brief Reflect the colour chosen by the scroll bars in the label
 * Draws a rectangle filled with the colour determined by the red, green and
 * blue scroll bars.
 */
void MainWidget::updateColour()
{
    // Get current RGB values.
    int red = ui->redBar_->value();
    int green = ui->greenBar_->value();
    int blue = ui->blueBar_->value();

    // Draw the rectangle.
    QSize size = ui->colourLabel_->size();
    QPixmap pixmap(size);
    QPainter paint(&pixmap);
    paint.fillRect(QRect(0, 0, size.width(), size.height()),
                   QBrush(QColor(red, green, blue)));

    // Display the image in the label.
    ui->colourLabel_->setPixmap(pixmap);
}
