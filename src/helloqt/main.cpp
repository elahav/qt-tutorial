/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QApplication>
#include <QPushButton>
#include "mylabel.h"
#include <QVBoxLayout>
#include <QHBoxLayout>

/**
 * @brief Program entry function
 * @param argc Number of command-line arguments
 * @param argv Array of command-line argument strings
 * @return 0 if successful, non-zero on error
 */
int main(int argc, char *argv[])
{
    // Construct the application object.
    QApplication a(argc, argv);

    // Construct a parent widget object.
    QWidget widget;

    // Add a push button and connect its clicked() signal to the application's
    // quit() slot. This will cause the application to exit when the button
    // is pressed.
    QPushButton *button = new QPushButton(&widget);
    button->setText("Quit");

    QObject::connect(button, SIGNAL(clicked()), &a, SLOT(quit()));

    // Add a custom label to display a numeric value.
    MyLabel *label = new MyLabel(&widget);

    // Add a push button and connect its clicked signal to the label's
    // increment() slot. This will cause the label to display a higher number
    // each time the button is pressed.
    QPushButton *button2 = new QPushButton(&widget);
    button2->setText("Increment");

    QObject::connect(button2, SIGNAL(clicked()),
                     label, SLOT(increment()));

    // Organize the child wwidgets inside the parent.
    // The two buttons are placed side-by-side.
    QHBoxLayout *hlayout = new QHBoxLayout;
    hlayout->addWidget(button2);
    hlayout->addWidget(button);

    // The label is placed above the two buttons.
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addLayout(hlayout);

    // Now associate this organization with the parent widget and make it
    // visible.
    widget.setLayout(layout);
    widget.setVisible(true);

    // Run the application's event loop.
    return a.exec();
}
