/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "counter.h"
#include <QHBoxLayout>

/**
 * @brief Class constructor
 * Creates the child widgets for the compound widget.
 * @param value     The initial value
 * @param parent    The parent widget
 */
Counter::Counter(int value, QWidget *parent)
    : QWidget{parent}, value_{value}
{
    // Create the label child widget.
    valueLabel_ = new QLabel(this);

    // Create the increment button and connect it to the increment slot of
    // the parent widget.
    incButton_ = new QPushButton("+", this);
    connect(incButton_, SIGNAL(clicked()), this, SLOT(increment()));

    // Create the increment button and connect it to the increment slot of
    // the parent widget.
    decButton_ = new QPushButton("-", this);
    connect(decButton_, SIGNAL(clicked()), this, SLOT(decrement()));

    // Position the child widgets inside the parent.
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(decButton_);
    layout->addWidget(valueLabel_);
    layout->addWidget(incButton_);
    setLayout(layout);

    update();
}

/**
 * @brief Show the current value in the label.
 */
void Counter::update()
{
    valueLabel_->setNum(value_);
}

/**
 * @brief Add one to the value and update the display.
 * This is a slot connected to the clicked() signal of the increment button.
 */
void Counter::increment()
{
    ++value_;
    update();
}

/**
 * @brief Subtract one from the value and update the display.
 * This is a slot connected to the clicked() signal of the decrement button.
 */
void Counter::decrement()
{
    --value_;
    update();
}
