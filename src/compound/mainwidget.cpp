/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mainwidget.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

/**
 * @brief Class constructor
 * Creates all of the child widgets for this window.
 * @param parent    The parent widget (should be nullptr)
 */
MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
{
    // Create the counter widgets.
    counterA_ = new Counter(10, this);
    counterB_ = new Counter(7, this);
    counterC_ = new Counter(3, this);

    // Create the sum label.
    result_ = new QLabel(this);

    // Create the sum button and connect its clicked() signal to the sum()
    // slot of this widget.
    QPushButton* button = new QPushButton("Sum", this);
    connect(button, SIGNAL(clicked()), this, SLOT(sum()));

    // Position the child widgets.
    QHBoxLayout *hlayout = new QHBoxLayout;
    hlayout->addWidget(result_);
    hlayout->addWidget(button);

    QVBoxLayout *vlayout = new QVBoxLayout;
    vlayout->addWidget(counterA_);
    vlayout->addWidget(counterB_);
    vlayout->addWidget(counterC_);
    vlayout->addLayout(hlayout);

    setLayout(vlayout);
}

/**
 * @brief Class destructor
 */
MainWidget::~MainWidget()
{
}

/**
 * @brief Calculate the sum of the three counter values and update the label.
 * This is a slot connected to the clicked() signal of the sum button.
 */
void MainWidget::sum()
{
    int result = counterA_->getValue() + counterB_->getValue()
                 + counterC_->getValue();
    result_->setNum(result);
}
